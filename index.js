const { Worker, workerData } = require('worker_threads');

module.exports = function (argWorker, argCapacity, argConcurrency = 1, argDelay = 0) {
  let options = {};
  let pendingTasks = [];

  if (arguments.length === 2) {
    if (typeof(arguments[0]) === 'function' && typeof(arguments[1]) === 'object' && arguments[1].capacity) {
      options = {
        worker: arguments[0],
        capacity: arguments[1].capacity,
        concurrency: arguments[1].concurrency || argConcurrency,
        delay: arguments[1].delay || argDelay
      }
    }
    else if (typeof(arguments[0]) === 'object' && arguments[0].capacity && typeof(arguments[1]) === 'function') {
      options = {
        worker: arguments[1],
        capacity: arguments[0].capacity,
        concurrency: arguments[0].concurrency || argConcurrency,
        delay: arguments[0].delay || argDelay
      }
    }
    else if (typeof(arguments[0]) === 'function' && typeof(arguments[1]) === 'number') {
      options = {
        worker: arguments[0],
        capacity: arguments[1],
        concurrency: argConcurrency,
        delay: argDelay
      }
    }
    else {
      throw new TypeError('Worker must be a function and Options must be a object with next args: capacity (required), concurrency (optional), delay (optional)');
    }
  }
  else if (arguments.length === 3) {
    if (typeof(arguments[0]) === 'function' && typeof(arguments[1]) === 'number' && typeof(arguments[2]) === 'number') {
      options = {
        worker: arguments[0],
        capacity: arguments[1],
        concurrency: arguments[2],
        delay: argDelay
      }
    }
    else {
      throw new TypeError('Worker must be a function, capacity must be a number, concurrency must be a number');
    }
  }
  else {
      if (typeof(argWorker) !== 'function') {
        throw new TypeError('Worker must be a function')
      }
      if (typeof(argCapacity) !== 'number' || typeof(argConcurrency) !== 'number' || typeof(argDelay) !== 'number') {
        throw new TypeError('Capacity, concurrency and delay must be a number')
      }
      options = {
          worker: argWorker,
          capacity: argCapacity,
          concurrency: argConcurrency,
          delay: argDelay
      }
  }

  let returnOptions = {
    length: 0,
    running: false,
    idle: true,
    concurrency: options.concurrency,
    push: (task) => {
      addTaskToWorkerQueue(task);
    }
  };

  let proxyReturnOptions = new Proxy(returnOptions, {
    get: function(target, key){
      return target[key];
    },
    set: function(target, key, value){
      if (key === "concurrency") {
        options.concurrency = value;
        target[key] = value;
      }
    }
  })

  const workers = [];

    for (let index = 0; index < options.concurrency; index++) {
      let worker = new Worker('('+options.worker+')()', { eval: true });
      worker.name = `worker ${index}`;
      worker.isRunning = false;
      worker.runningTasks = 0;
      worker.tasksArray = [];
      workers.push(worker);
    }

    workers.forEach(worker => {
      worker.on('message', function(e) {
        worker.runningTasks--;
        if (worker.runningTasks === 0) {
          worker.isRunning = false;
          if (!!workers.every(worker => worker.isRunning === false)) {
            returnOptions.running = false;
            if (workers.every(worker => worker.tasksArray.length === 0) && pendingTasks.length === 0) {
              returnOptions.idle = true;
            }
          }
          addPendingTasksToWorkersQueue();
        }
      });
    })

    function addPendingTasksToWorkersQueue() {
      const isOneOfWorkersIsNotRunning = workers.some(worker => worker.isRunning === false);

      if (pendingTasks.length > 0 && isOneOfWorkersIsNotRunning) {
        addTaskToWorkerQueue(pendingTasks[0], true);
        pendingTasks.shift();
        addPendingTasksToWorkersQueue();
      }
    }

    function runWorker(worker) {
      worker.tasksArray.forEach(task => {
        worker.postMessage(task);
        worker.runningTasks++;
      })
      returnOptions.idle = false;
      worker.isRunning = true;
      returnOptions.running = true;
      returnOptions.length -= worker.tasksArray.length;
      worker.tasksArray = [];
    }

    function addTaskToWorkerQueue(task, ifTaskReceivedFromPending) {
      let selectedWorker = null;

      // choose free worker with maximum task filling
      selectedWorker = workers.reduce((acc, next) => {
        if (!next.isRunning && Math.max(acc && acc.tasksArray.length, next.tasksArray.length) === next.tasksArray.length) {
          return next;
        }
        else {
          return acc;
        }
      });

      // if, before pushing task to array, choosed worker have 'capacity' tasks, add task to pendingTasks array
      if (selectedWorker.tasksArray.length === options.capacity || selectedWorker.isRunning) {
        pendingTasks.push(task);
        return;
      }

      selectedWorker.tasksArray.push(task);

      // if, after pushing task to array, choosed worker have 1 task in queue set timeout with delay
      // in setTimeout function, if choosed worker isn't running, run worker
      if (selectedWorker.tasksArray.length === 1) {
        setTimeout(() => {
          if (!selectedWorker.isRunning) {
            runWorker(selectedWorker);
          }
        }, options.delay)
      }

      // if, after pushing task to array, choosed worker have 'capacity' tasks, run worker
      if (selectedWorker.tasksArray.length === options.capacity) {
        runWorker(selectedWorker);
      }

      if (!ifTaskReceivedFromPending) {
        returnOptions.length++;
      }
      returnOptions.idle = false;
    }
    
    return proxyReturnOptions;
  }
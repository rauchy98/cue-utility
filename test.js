const mocha = require('mocha');
const chai = require('chai');
const assert = require('assert');
const cue = require('./index.js');

function myWorker () {
    const { workerData, parentPort } = require('worker_threads');
    parentPort.postMessage({ status: 'Done' })
}

describe("init", function(){
    it('init with worker and capacity args; should return object without errors', function() {
        const myCue = cue(myWorker, 5);
        assert.equal(typeof(myCue), 'object');
    });
    it('init with worker, capacity and concurrency args; should return object without errors', function() {
        const myCue = cue(myWorker, 5, 2);
        assert.equal(typeof(myCue), 'object');
    });
    it('init with worker, capacity, concurrency and delay args; should return object without errors', function() {
        const myCue = cue(myWorker, 5, 2, 2);
        assert.equal(typeof(myCue), 'object');
    });
    it('init with worker and options args; should return object without errors', function() {
        const myCue = cue(myWorker, {
            capacity: 5,
            concurrency: 2,
            delay: 2
        });
        assert.equal(typeof(myCue), 'object');
    });
    it('init with options and worker args; should return object without errors', function() {
        const myCue = cue({
            capacity: 5,
            concurrency: 2,
            delay: 2
        }, myWorker);
        assert.equal(typeof(myCue), 'object');
    });
});

describe("functionality", function() {

    it('check concurrency setting', function() {
        const myCue = cue(myWorker, 5);
        myCue.concurrency = 3;
        assert.equal(myCue.concurrency, 3);
    })

    it('check running bool', function() {
        const myCue = cue(myWorker, 3);
        myCue.push('1');
        myCue.push('2');
        myCue.push('3');
        myCue.push('3');
        myCue.push('3');
        myCue.push('3');
        myCue.push('3');
        myCue.push('3');
        myCue.push('3');
        assert.equal(myCue.running, true);
    })

    it('check push func and length counter', function() {
        const myCue = cue(myWorker, 5, 1, 5000);
        myCue.push('1');
        myCue.push('2');
        assert.equal(myCue.length, 2);
    })

    it('check idle bool 1', function() {
        const myCue = cue(myWorker, 4);
        myCue.push('1');
        assert.equal(myCue.idle, false);
    })

    it('check idle bool 2', function() {
        const myCue = cue(myWorker, 1);
        myCue.push('1');
        setTimeout(() => {
            assert.equal(myCue.idle, true);
        }, 1000)
    })
})